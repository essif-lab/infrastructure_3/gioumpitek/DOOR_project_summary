# DOOR Project Summary

For more information, please contact:
Thanassis Giannetsos, agiannetsos@ubitech.eu
or Ioannis Krontiris, ioannis.krontiris@huawei.com

## Introduction

### About 
The Security & Trusted Computing Research Group (DST) of UBITECH (https://ubitech.eu/) in Greece, focuses on research activities related to trust management in various application domains. DST’s members have extensive experience in EU projects on topics related to assurance levels in SSI through integration of trusted computing technologies. The DOOR project is also supported by Huawei Research, Munich with expertise in identity management and usage control systems based on verifiable credentials.

### DOOR - Hardware Roots of Trust as an Enabler of Trustworthiness in Digital Transactions	

DOOR empowers participating entities in the ESSIF-Framework to establish trust by providing strong verifiable evidence and assurances on the origin and integrity of the presented verifiable credentials. DOOR achieves this goal by building a trusted layer between interacting parties that is based on strong cryptographic privacy-preserving tools. In this way, DOOR can enable the handling of VCs requiring higher level of assurances locally at the holder’s wallet. 

We achieve the above goal by providing a new component on the Holder side that enables the use of hardware-based keys and offers the possibility to bind Verifiable Credentials (VCs) to the wallet of the holder. In this way, we transfer the root of trust of the SSI ecosystem purely on the digital wallet by considering an underlying Trusted Component as part of the wallet, without making any assumptions on the trustworthiness of the other layers. 

Innovators can build on our extension to integrate hardware-based keys and bring trust to the holder’s wallets, thus, enabling the vision of a decentralized data protected approach for Self-Sovereign Identity (SSI). Since the DOOR component is agnostic to the implementation of the wallet and the type of the VC Data Model considered (e.g. W3C or Indy), multiple SSI ecosystems can interoperate in a trustworthy manner. Overall, innovators can use our component to create a hardware-based trust anchor and build services that offer a high level of assurance. 


## Summary

### Business Problem

In a basic SSI architecture, one core challenge is the verification of the integrity and origin of the presented Verifiable Credentials (VCs): How can someone be sure that the presented VCs (for a specific DID) realy belong to the claimed entity? Of particular interest is the case of the Holder, since SSI brings users in the center and gives them full control over their identity (selective disclosure). On a technical level, this translates to users having control of their own VCs and DIDs through their Wallets which can ensure that credentials and (private) keys can only become available to this user or another actor that acts on behalf of this principal. However, since it is only the user (as the Identity Owner) that knows the (private) key associated with a DID, the level of control and assurance over a DID (and a VC) relies solely on possessing a software-based key, creating trustworthiness issues: How can a Verifier be sure of the correctness of the User/Holder that presents a VC that the respective key has not been compromised?


### Technical Solution

DOOR will provide a component, which we call "DAA-Bridge", which will generate hardware-based keys and bind them to a Holder’s VC (Wallet) and so provide verifiable evidence and assurances about the presented VC’s origin and integrity. Internal strong crypto operations of DAA (blind signatures, zero knowledge proofs) enable DOOR to minimize the data disclosed in a given transaction. Our DAA-bridge extension shifts the RoT on the user digital wallet, thus achieving the requirement of selective disclosure.

Different attestation software stacks, comprising the DAA-bridge (ranging from Z-CIV to DAA) provide verifiable evidence on the correct state of the Holder’s device/wallet including measurements on identity and information about the software running. Besides having strong assertions on the integrity of the ID, claimed by an entity, the advanced crypto techniques of DAA-ECCs coupled with the integration of hw-based keys enable counterparties to establish “Chains-of-Trust” and associated certification: By binding a verifiable credential that can only be issued by an attested Issuer, to a Holder’s device/wallet so that it can only be used through a certified DAA-bridge. This overcomes the current limitations of bare proof-of-possession of a software-based key which does not achieve even the lowest level of assurance.

DOOR is agnostic of the wallet’s implementation and the underlying VC Data Model considered. 
